﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AcademicManagement.Startup))]
namespace AcademicManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
